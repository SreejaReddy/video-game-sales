#Video game Sales

** Project Name :** MapReduce on Video game sales  
** Course       :** 44564_01-Data Intensive Systems  
** Project group:** 1E
## Collaborators##
* Project pair 1-9
    * Lavanya Kalluri   
        * Email: S528739@nwmissouri.edu  
    * Sreevani Anoohya Tadiboina  
        * Email: S530488@nwmissouri.edu  
* Project pair 1-10
    * Sreeja Reddy Challa  
        * Email: S528736@nwmissouri.edu  
    * Sai Devi Uppalapati  
        * Email: S528765@nwmissouri.edu  

##Useful links for repository##
* Link to repository: https://bitbucket.org/SreejaReddy/video-game-sales/overview
* Link to issue tracker: https://bitbucket.org/SreejaReddy/video-game-sales/issues?status=new&status=open

##Introduction##
This project involves a dataset which contains data related to video game sales of different countries and also global sales based on various video game platforms from 1981-2018. All the sales are in millions of dollars.The goal of this project is to calculate the total sales in different countries and analyze which video game has highest sales over a period of time. To do the required calculations and analysis we are using Hadoop MapReduce using Python.

##Data Source##
**File extension:** .csv  
**Size:** 1.29 MB  
**Rows:** 16598  
**Columns:** 11 columns    

* Column meta data    
    * Rank - Ranking of overall sales    
    * Name - The games name    
    * Platform - Platform of the games release (i.e. PC,PS4, etc.)    
    * Year - Year of the game's release    
    * Genre - Genre of the game    
    * Publisher - Publisher of the game    
    * NA_Sales - Sales in North America (in millions)    
    * EU_Sales - Sales in Europe (in millions)    
    * JP_Sales - Sales in Japan (in millions)    
    * Other_Sales - Sales in the rest of the world (in millions)    
    * Global_Sales - Total worldwide sales    

**Type:** Structured data    
**Link to data source:** https://www.kaggle.com/gregorut/videogamesales/data
If there is any error found in the dataset preview please download to see the data or use below link for the dataset
https://www.kaggle.com/rush4ratio/video-game-sales-with-ratings/data

##What makes it a Big data problem?##
**Volume:** The dataset is 1.29 MB in size. In the perspective of big data, this a medium volume data. This dataset contains 16,598 rows of video game sales related data with 11 columns.  
**Variety:** This is structured data with 11 columns related to video game sales, platform, genre, year of release and publicher of each game.  
**Velocity:** This does not have high velocity, as it gives year wise sales and rankings from 1981-2018. Consider this as Data-at-rest.    
**Veracity:** This data is properly organized and accurate. There are no uncertainities found in the dataset.    
**Value:** Using this data, we can analyse some of the aspects of video game sales such as predict the sales of a particular video game in various regions across the globe. We can also know which publisher is most popular and which platform-based video games are giving highest sales globally or in a particular region.  

##Big data questions##
* Question1-Sreeja: For each product, find total global sales
* Question2-Sai Devi: For each product, find minimum and maximum sales globally
* Question3-Lavanya: For each platform, find total sales of products in North America
* Question4-Srevani Anoohya:For each platform, find maximum and minimum sales of products in North America

##Big data solutions##
###Question1-Sreeja:  
**Information about solution**  

* For each product, total global sales is caluculated and top 10 video game sales is shown in a column chart.   
* Commands to run mapper locally  

    * Open the source code in visual studio code  
    * In the visual studio terminal enter 'cd Sreeja'  
    * Enter 'python sreeja_mapper.py' to run mapper 
    * Check 'sreeja_mapperOutput.txt' file for mapper output  
    * Enter 'python sreeja_sorter.py' to run sorter 
    * Check 'sorterOutput.txt' file for sorted output.   
    * Enter 'python sreeja_reducer.py' to run reducer  
    * To check final map reduce results open 'sreeja_reducerOutput.txt' file
    
**Sample input and output**
**Mapper input:**  

| Rank	| Name	    | Platform | Year	| Genre	   | Publisher	|NA_Sales	| EU_Sales	| JP_Sales	|Other_Sales	|Global_Sales
| ------|-----------|----------|--------|----------|------------|-----------|-----------|-----------|---------------|-------
|259	| Asteroids	| 2600	   | 1980	| Shooter  |   Atari	|    4	    |   0.26	|   0	    |    0.05	    | 4.31

**Mapper output:** Asteroids 4.31  
**Reducer input:** Asteroids 4.31  
**Reducer output:** Asteroids Total: 4.31M  
**Programming language:** Python  
**Final result image:**

![Sreeja_output.PNG](https://bitbucket.org/repo/ypn9qp7/images/2211220240-Sreeja_output.PNG)

**Type of chart to display results:** A column chart to show top 10 video game sales globally
###Graphical representation of results 
The garph is generated using Microsoft Excel. Refer to 'Results.xslx' file in the 'Sreeja' folder to see the records used to generate the chart.   

![Top10 Global_Sales.PNG](https://bitbucket.org/repo/ypn9qp7/images/860115490-Top10%20Global_Sales.PNG)  

###Question2-Saidevi  

* For each product, find maximum global sales.
* Commands to run mapper locally  

  * Open the source code folder in visual studio code 
  * Enter 'python Saidevi_mapper.py' in the terminal to run mapper
  * Check mapperOut.txt file for the key-value pairs
  * Enter 'python Saidevi_sorter.py' to run sorter file
  * Check sorterOut.txt file for sorter output.
  * Enter 'python Saidevi_reducer.py' to run reducer file
  * Check 'reducerOut.txt' file for final map reduce results

**Sample input and output**
**Mapper input:** 

| Rank	| Name	    | Platform | Year	| Genre	   | Publisher	|NA_Sales	| EU_Sales	| JP_Sales	|Other_Sales	|Global_Sales
| ------|-----------|----------|--------|----------|------------|-----------|-----------|-----------|---------------|-------
|259	| Asteroids	| 2600	   | 1980	| Shooter  |   Atari	|    4	    |   0.26	|   0	    |    0.05	    | 4.31

**Mapper output:** Asteroids 4.31  
**Reducer input:** Asteroids 4.31  
**Reducer output:** Asteroids maximum: 4.31M   
**Programming language:** Python    
**Type of chart to display results:** A Bar chart to show the maximum global_sales made by the top 10 video games over the period 1981-2018.  
**Results image:**
![Saidevi_reducer.PNG](https://bitbucket.org/repo/ypn9qp7/images/2227883175-Saidevi_reducer.PNG)

###Graphical Representation of results 
I used Microsoft Excel to generate graph from reducer output.
![MaxGlobal_sales.PNG](https://bitbucket.org/repo/ypn9qp7/images/285783313-MaxGlobal_sales.PNG)

###Question3-Lavanya  

* For each platform, find total sales of products in North America  
* Commands to run mapper locally  

    * Open the source code in visual studio code  
    * In the visual studio terminal enter 'cd Lavanya'  
    * Enter 'python Lavanya_mapper.py'  
    * Check opt.txt file for mapper output  
    * Enter 'python Lavanya_sorter.py' to run 'Lavanya_sorter.py' file   
    * Check sorted.txt file for sorter output. In this file the mapper output is sorted alphabetically.  
    * Enter 'python Lavanya_reducer.py' to run reducer file
    * To check final map reduce results open 'output.txt' file  
    
**Sample input and output**
**Mapper input:**  

| Rank	| Name	    | Platform | Year	| Genre	   | Publisher	|NA_Sales	| EU_Sales	| JP_Sales	|Other_Sales	|Global_Sales
| ------|-----------|----------|--------|----------|------------|-----------|-----------|-----------|---------------|-------
|259	| Asteroids	| 2600	   | 1980	| Shooter  |   Atari	|    4	    |   0.26	|   0	    |    0.05	    | 4.31

**Mapper output:** 2600 4  
**Reducer input:** 2600 4  
**Reducer output:** Platform 2600 video game total sales in North America is 4M  
**Programming language:** Python 

**Final result image:**

![LavanyaFinalOutput.png](https://bitbucket.org/repo/ypn9qp7/images/2185648133-LavanyaFinalOutput.png)
 
**Type of chart to display results:** A Tree Map to show total sales of top 20 platforms in North America  
###Graphical representation of results 

![Top 20 Platfoms NA Sales.PNG](https://bitbucket.org/repo/ypn9qp7/images/2666522940-Top%2020%20Platfoms%20NA%20Sales.PNG)

###Question4- Sreevani Anoohya  
For each platform, find maximum and minimum sales of products in North America  
**Information about solution**  

* Commands to run mapper locally  

    * Open the source code in visual studio code  
    * In the visual studio terminal enter 'cd Sreevani'  
    * Enter 'python sreevani_mapper.py'  
    * Check mapperinput.txt file for mapper output  
    * Enter 'python sreevani_sorter.py' to run 'sreevani_sorter.py' file   
    * Check sorteddata.txt file for sorter output. In this file the mapper output is sorted alphabetically.  
    * Enter 'python sreevani_reducer.py' to run reducer file
    * To check final map reduce results open 'reducerouput.txt' file  
    
**Sample input and output**  

**Mapper input:**  

| Rank	| Name	    | Platform | Year	| Genre	   | Publisher	|NA_Sales	| EU_Sales	| JP_Sales	|Other_Sales	|Global_Sales
| ------|-----------|----------|--------|----------|------------|-----------|-----------|-----------|---------------|-------
|259	| Asteroids	| 2600	   | 1980	| Shooter  |   Atari	|    4	    |   0.26	|   0	    |    0.05	    | 4.31

**Mapper output:** 2600 4  
**Reducer input:** 2600 4  
**Reducer output:** The maximum and minimum values of NA sales:  
Name of the platform:2600  
Maximum sales:4  
Minimum sales:4  
**Programming language:** Python  
**Final result image:**   

![Sreevani_output.png](https://bitbucket.org/repo/ypn9qp7/images/1751051959-Sreevani_output.png)

**Type of chart to display results:** A Line chart to show maximum and minimum number of video game sales in North America based on platform  
###Graphical representation of results  
I used Microsoft Excel for generating the visual representation of maximum and minimum sales of NA for each platform  

![Max&Min.png](https://bitbucket.org/repo/ypn9qp7/images/1790589317-Max&Min.png)
