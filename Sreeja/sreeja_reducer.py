# 
reducerIn = open("sorterOut.txt","r")  # open file, read-only
reducerOut = open("reducerOut.txt", "w") # open file, write 

thisKey = ""
thisValue = 0.0

for line in reducerIn:
	data = line.strip().split('\t')
	# Check for valid input
	if len(data) == 2:
		Name, Global_Sales = data  # read into variables

	if Name != thisKey:
		if thisKey:
			# output the final result
			reducerOut.write(thisKey +" \t "+ str(thisValue)+'\n')
			#print(thisKey +" has total sales of "+ str(thisValue)+ "M"+'\n')

		# Assigning game name to the key whenever the game name is changeds
		thisKey = Name
		thisValue = 0.0

	# calculate the total cost
	thisValue += float(Global_Sales)

# to output the last line of the file
#reducerOut.write(thisKey +" has total sales of "+ str(thisValue)+ "M"+'\n')
reducerOut.write(thisKey +" \t "+ str(thisValue)+'\n')

reducerIn.close() # to close all open files
reducerOut.close()
