read_file = open("vgSales.txt","r") # open file, read-only
write_file = open("mapperOut.txt", "w") # open file, write

for line in read_file.readlines(): # to read each line in the dataset
    data = line.strip().split("\t") # Split each file on tab space
    if len(data) == 11:
        # Assigning names to columns
        Rank, Name, Platform, Year, Genre, Publisher, NA_Sales, EU_Sales, JP_Sales, other_Sales, Global_Sales = data
        write_file.write(Name+'\t'+Global_Sales+'\n')
        #print(Name+'\t'+Global_Sales+'\n')

read_file.close() # to close the files which are opened 
write_file.close()