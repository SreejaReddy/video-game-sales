# This is reducer file. Perform maximum function on the sorted data.

# open file, read only
sorted_data = open("sorterOut.txt","r") 
# open file. write
reduced_data = open("reducerOut.txt", "w")

thisKey = ""
thisValue = 0.0
count = 0

# for each line in sorted_data
for line in sorted_data:
  #cleans the line and split it according to tabs
  data = line.strip().split('\t')
  #assigns Name and Global_Sales to data
  Name, Global_Sales = data

  if Name != thisKey:
    if thisKey:
      # output the key value pair to reduced_data
      reduced_data.write(thisKey + '\t'+ str(max) + '\t' + '\n')


    thisKey = Name 
    thisValue = 0.0
    max = 0
  # max function  
  if max < float(Global_Sales):
    max = float(Global_Sales)
 
    
reduced_data.write(thisKey + '\t'+ str(max) + '\t' + '\n')

# closes sorted_data and reduced_data
sorted_data.close()
reduced_data.close()
