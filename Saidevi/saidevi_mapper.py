#This is the mapper file. It is used to map the dataset into Key-Value pairs.
# Here the key is Name and value is Global_sales


# open file vgSales.txt, "r" specifies the access. Here we have given read only access
read_file = open("vgSales.txt","r")
# open file mapperOut.txt to write, assign it to write_file
write_file = open("mapperOut.txt", "w") 

# reads each line in read_file
for line in read_file.readlines():
    # strips the data and splits based on tabs
    data = line.strip().split("\t")
    # check for valid input
    if len(data) == 11:

        #read all the columns into data
        Rank, Name, Platform, Year, Genre, Publisher, NA_Sales, EU_Sales, JP_Sales, other_Sales, Global_Sales = data
        # write Names, Global_Sales into write_file
        write_file.write("{0}\t{1}\n".format(Name,Global_Sales))
         
#close the files        
read_file.close()
write_file.close()