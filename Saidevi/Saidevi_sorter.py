# This is sort file. Takes mapper key value pairs as input, sorts the data.

sorterIn = open("mapperOut.txt","r")  # open file, read-only
sorterOut = open("sorterOut.txt", "w") # open file, write
# reads sorterIn into lines
lines = sorterIn.readlines()
# sorts the data in lines
lines.sort()
#print(lines)
# for each line in lines, perform the write function
for line in lines:
	sorterOut.write(line)
#close the files  
sorterIn.close()
sorterOut.close()