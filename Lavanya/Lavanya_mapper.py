file = open("vgSales.txt","r") # opens and reads file vgsales
opt = open("opt.txt", "w") # opens and writes to file opt

 

for line in file.readlines(): # reads lines from input file
    data = line.strip().split("\t")
    if len(data) == 11: # checks length of the data 
    # assigning each column to data variable
        Rank, Name, Platform, Year, Genre, Publisher, NA_Sales, EU_Sales, JP_Sales, other_Sales, Global_Sales = data
        # writing output to opt file
        opt.write("Platform: {0}\t Sales: {1}\n".format(Platform, NA_Sales))
       # print ("Platform: {0}\t Sales: {1}\n".format(Platform, NA_Sales))
file.close() # closes file 
opt.close() # closes opt