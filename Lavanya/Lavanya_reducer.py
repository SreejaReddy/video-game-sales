input = open("sorted.txt", "r") # opens and reads file sorted
output = open("output.txt", "w") # opens and writes to file output

thisKey = ""
thisValue = 0.0
for line in input: # reads lines of input
 data = line.strip().split('\t')
 Platform, NA_Sales = data # assigns columns to data 

 if Platform != thisKey:
   if thisKey:
     # writing output to output file
     output.write("Platform: "+thisKey + '\t'+ "Sales: " + str(thisValue)+'\n')
    # print "Platform: "+thisKey + '\t'+ "Sales: " + str(thisValue)

# assigning platform to key    
   thisKey = Platform
   thisValue = 0.0
 # calculate total
 thisValue += float(NA_Sales)
 # writing output to output file
output.write("Platform: "+thisKey + '\t'+ "Sales: " + str(thisValue)+'\n')

input.close() # closes input file
output.close() # closes output file


